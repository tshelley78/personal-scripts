#!/bin/bash

#
# Name: Tom Shelley
# Date Created: March 10, 2021
# Date Modified: Novemnber 19, 2021
# Description:
# Display all screens at start up
#

line=$(xrandr --listproviders | grep "number : ")
count=${line: -1}
if (($count > 1)); then
	for (( i=1; i<$count; i++))
	do
		xrandr --setprovideroutputsource $i 0
	done
fi
sleep 5

displayConfig=$(xrandr | grep -w "connected" | wc -l)
if [ "$displayConfig" -gt "1" ]; then
    xrandr --output DVI-I-2-2 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DVI-I-1-1 --mode 1920x1080 --pos 1920x0 --rotate normal --output eDP-1 --mode 1920x1080 --pos 3840x0 --rotate normal
fi
