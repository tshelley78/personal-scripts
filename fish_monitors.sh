#!/bin/fish

#
# Name: Tom Shelley
# Date Created: March 12, 2021
# Date Modified: March 12, 2021
# Description:
# Try to display all screens at start up
#

set -g line (xrandr --listproviders | grep "number : ")
set -g count (string sub -s -1 $line)

if test "$count" -gt 1
    for i in (seq 1 (math "$count-1"))
        xrandr --setprovideroutputsource $i 0
    end
end

source ~/.screenlayout/primary_layout.sh
