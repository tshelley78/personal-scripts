# suppress fish shell greeting
set -U fish_greeting

# add starship run prompt
starship init fish | source

# alias for poweroff, reboot, suspend
alias poweroff 'sudo /usr/bin/systemctl poweroff'
alias reboot 'sudo /usr/bin/systemctl reboot'
alias suspend 'sudo /usr/bin/systemctl suspend'

# call monitor startup script
#source ~/scripts/fish_monitors.sh

# asdf
source ~/.asdf/asdf.fish
