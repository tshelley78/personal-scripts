" Author: Tom Shelley
" Date Created: March 11, 2021
" Date Modified: March 11, 2021
" My .vimrc file


set number
syntax on
set autoindent
set expandtab
set softtabstop=4
set cursorline

